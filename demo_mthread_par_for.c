#include <stdio.h>
#include <stdlib.h>
#include "./mthread/mthread.h"
#include <assert.h>

int verbose_mode = 1;

int NB_THREADS;
int race_var=0;


void* func_test_thread (void* arg) {
    if(verbose_mode)printf("func_test_thread %ld : Entering\n",(long)arg);
    long rank;
    rank = (long)arg;

    if(verbose_mode)printf("---> Hello, I’am %ld (%p)\n",rank,mthread_self());

    if(verbose_mode)printf("func_test_thread %ld :Exiting\n", (long)arg);
    return arg;
    }






int main (int argc, char **argv) {
    printf("-----------------------\n");
    printf("Demonstration problem\n");
    printf("-----------------------\n");

    int ret;
    int arg = 1;
    int nb_threads = 4;
    int nb_loops = 5;
    void **res;
    int mode = 0; //1 dynamic mode, 0 static mode
    printf("Using %d threads, executing %d loops\n",nb_threads, nb_loops);
    ret = mthread_parallel_for(NULL,func_test_thread,(void*)arg,nb_threads,nb_loops,mode,&res,verbose_mode);

    if(res == 0) printf("Well, fine until now\n");
    for(int j=0; j<nb_loops; j++) {
        printf("demo_mthread_par_for : Loop %d : result : %ld\n",j,(long)(res[j]));
        }

    printf("-----------------------\n");
    return 0;
    }
