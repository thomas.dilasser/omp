#include <stdio.h>
#include <stdlib.h>
#include "./mthread/mthread.h"
#include <assert.h>

int with_mutex = 1;
int verbose = 0;

mthread_mutex_t* mutex;
int NB_THREADS;
int race_var=0;


void* func_test_thread (void* arg) {
    if(verbose)printf("func_test_thread %ld : Before locking\n",(long)arg);
    if(with_mutex) {
        if(mthread_mutex_lock(mutex)!= 0) printf("Lock problem\n");
        }
    long rank;
    rank = (long)arg;

    if(verbose)printf("Hello, I’am %ld (%p)\n",rank,mthread_self());
    for(int i=0; i< 1000000; i++) {
        race_var++;
        }

    if(with_mutex) {
        if(mthread_mutex_unlock(mutex)!= 0) printf("Unlock problem\n");
        }
    if(verbose)printf("func_test_thread %ld : Unlocked, Exiting\n", (long)arg);
    return arg;
    }






int main (int argc, char **argv) {
    printf("Demonstration problem\n");
    mthread_t* pids;
    long i;
    int ret;
    NB_THREADS=atoi(argv[1]);

    if(verbose)printf("Main : Initializing mutex\n");
    mutex = (mthread_mutex_t*)malloc(sizeof(mthread_mutex_t));
    ret = mthread_mutex_init(mutex,NULL);
    mthread_mutex_init(mutex,NULL);

    pids = (mthread_t*)malloc(NB_THREADS*sizeof(mthread_t));
    for(i=0; i< NB_THREADS; i++) {
        if(verbose)printf("Main : creating thread %ld\n",i);
        mthread_create(&(pids[i]),NULL,func_test_thread,(void*)i);
        }
    void* res;
    for(i=0; i<NB_THREADS; i++) {
        mthread_join(pids[i],&res);
        //fprintf(stderr,"Thread %ld Joined\n",(long)res);
        assert(res==(void*)i);
        }
    printf("Main : final sum %d\n",race_var);
    ret = mthread_mutex_destroy(mutex);
    if(verbose)printf("Main : Mutex destroyed\n");
    return 0;
    }
