#include <errno.h>
#include "mthread_internal.h"



/* Functions for mutex handling.  */

/* Initialize MUTEX using attributes in *MUTEX_ATTR, or use the
   default values if later is NULL.  */
int
mthread_mutex_init (mthread_mutex_t * __mutex,
                    const mthread_mutexattr_t * __mutex_attr) {
    //On néglige l'attribut
    if(__mutex_attr != NULL) {
        printf("mutexattr : not implemented\n");
        }
    else {
        __mutex->nb_thread=0;//nb_thread à 0 car pas de threads au début
        __mutex->lock = 0;//lock du mutex à 0 car pas lock au debut
        __mutex->list = (mthread_list_t*)malloc(sizeof(mthread_list_t));//Allocation de la mémoire de la structure liste
        __mutex->list->first = NULL;//initialisation des 3 éléments avec les memes attributs que dans le mthread.c
        __mutex->list->last = NULL;
        __mutex->list->lock = 0;

        }
    return 0;
    }

/* Destroy MUTEX.  */
int
mthread_mutex_destroy (mthread_mutex_t * __mutex) {
    free(__mutex->list);//On libère la mémoire de la liste
    __mutex->nb_thread=0;
    return 0;
    }

/* Try to lock MUTEX.  */
int
mthread_mutex_trylock (mthread_mutex_t * __mutex) {
    not_implemented ();
    return 0;
    }

/* Wait until lock for MUTEX becomes available and lock it.  */
int
mthread_mutex_lock (mthread_mutex_t * __mutex) {
    mthread_spinlock_lock(&(__mutex->lock));//On enclenche le spinlock jusqu'à une sortie de la fonction

    if(__mutex->nb_thread == 0) {//Si c'est le premier thread à atteindre la fonction
        __mutex->nb_thread = 1;
        mthread_spinlock_unlock(&(__mutex->lock));//On sort de la fonction juste après
        }
    else {

        mthread_insert_last(mthread_self(),__mutex->list);//Puisque ce n est pas le premier on l ajoute à la fin de la liste d'attente
        mthread_self()->status = BLOCKED;//le thread étant entré, lui assigne le bon statut
        __mutex->nb_thread++; //Un thread de plus en attente
        mthread_get_vp()->p = &__mutex->lock;//On assigne le lock au processeur virtuel
        mthread_yield();//On relâche la ressource et cela enclenche spinunlock également
        }
    return 0;
    }

/* Unlock MUTEX.  */
int
mthread_mutex_unlock (mthread_mutex_t * __mutex) {
    struct mthread_s *removed_thread;//déclaration de la structure qui va permettre de récupérer le prochain thread à récupérer
    mthread_spinlock_lock(&(__mutex->lock));//verrouillage de la section d'instruction

    if(__mutex->list->first != NULL) { // Si il y a un thread à débloquer

        removed_thread = mthread_remove_first(__mutex->list);//on le récupère
        removed_thread->status = RUNNING;//on lui assigne le statut actif
        mthread_insert_last(removed_thread,&mthread_get_vp()->ready_list);//on le rajoute dans la liste d'attente des ressources
        __mutex->nb_thread--;
        }
    else { //si il n'y a pas de threads bloqués
        __mutex->nb_thread = 0;
        }

    mthread_spinlock_unlock(&(__mutex->lock));//déverrouillage
    return 0;
    }
